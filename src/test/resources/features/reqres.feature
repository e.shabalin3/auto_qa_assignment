Feature: Reqres API for user creation and registration

  Scenario Outline: Create user with certain user name and job
    When I call user creation endpoint for user with name <User name> and job <Job>
    Then the resulting response should have Created status code, ID and created date field
    Examples:
      | User name | Job         |
      | mike      | asdasf      |
      | @123asd": | ":1e123asd" |
      |           |             |

  Scenario: Register user using email and password
    When I call register user endpoint with email lindsay.ferguson@reqres.in and password asdafasdas
    Then the resulting response should have OK status code, ID and token

  Scenario Outline: Register user without specifying email or password (negative scenario)
    When I call register user endpoint with email <Email> and password <Password>
    Then the resulting response should have Bad request status code and error message <Error Message>
    Examples:
      | Email     | Password | Error Message             |
      |           | asdasf   | Missing email or username |
      |           |          | Missing email or username |
      | @123asd": |          | Missing password          |