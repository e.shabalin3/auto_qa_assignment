package reqres;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.*;

public class ReqresStepDefinitions {

    @Steps
    private ReqresAPI reqresAPI;

    @When("I call user creation endpoint for user with name {} and job {}")
    public void createUser(final String userName, final String job) {
        reqresAPI.createUserEndpointCall(userName, job);
    }

    @When("I call register user endpoint with email {} and password {}")
    public void registerUser(final String email, final String password) {
        reqresAPI.registerUserEndpointCall(email, password);
    }

    @Then("the resulting response should have Created status code, ID and created date field")
    public void theUserCreationResultingResponseShouldHave() {
        restAssuredThat(response -> response.statusCode(201));
        restAssuredThat(response -> response.body("id", not(emptyOrNullString())));
        restAssuredThat(response -> response.body("createdAt", not(emptyOrNullString())));
    }

    @Then("the resulting response should have OK status code, ID and token")
    public void theUserRegisterResultingResponseShouldHave() {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body("id", not(emptyOrNullString())));
        restAssuredThat(response -> response.body("token", not(emptyOrNullString())));
    }

    @Then("the resulting response should have Bad request status code and error message {}")
    public void theUserFailedRegisterResultingResponseShouldHave(final String errorMessage) {
        restAssuredThat(response -> response.statusCode(400));
        restAssuredThat(response -> response.body("error", equalTo(errorMessage)));
    }
}
