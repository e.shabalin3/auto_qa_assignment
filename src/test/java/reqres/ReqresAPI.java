package reqres;

import io.cucumber.java.Before;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.parsing.Parser;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import reqres.models.User;


public class ReqresAPI {
    @Before
    public void configureBaseUrl() {
        SerenityRest.setDefaultRequestSpecification
                (RestAssured.requestSpecification = new RequestSpecBuilder()
                        .setBaseUri(("https://reqres.in/api"))
                        .setAccept("*/*")
                        .setContentType("application/json")
                        .setRelaxedHTTPSValidation()
                        .build()
                );
        SerenityRest.setDefaultParser(Parser.JSON);
        SerenityRest.enableLoggingOfRequestAndResponseIfValidationFails();
    }

    @Step("Create user POST with name {0} and job {1}")
    public void createUserEndpointCall(final String userName, final String job) {
        User user = new User();
        user.setName(userName);
        user.setJob(job);
        SerenityRest.given().body(user).post("/users");
    }

    @Step("Register user POST with email {0} and password {1}")
    public void registerUserEndpointCall(final String email, final String password) {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        SerenityRest.given().body(user).post("/register");
    }
}
