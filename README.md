# api_auto_qa_assignment
## Getting started
1. Install Java 8 (1.8 sdk/jre)
2. Install IntelliJ IDEA (preferable 2020.2.4 (Community Edition))
3. Install Lombok plugin inside and Enable annotation processing (Settings -> Annotation processors) for it (see https://projectlombok.org/ for more information)
4. (Optional) You might want to install gherkin plugin for cucumber steps connection to definitions
## Run tests
1. You can run tests using JUnit runner on CucumberTestSuite.class or using _gradle test_ task
2. All information about test run will be displayed in logs via logback logger
3. Report generation - gradle aggregate (build tool is configured to gerenate report after testing is done)
4. You can find generated report in target/site/serenity/index.html (open with browser)
## Add new tests
1. Add cucumber feature file with scenarios in test/resources/features catalog
2. Add definitions for your steps in ReqresStepDefinitions.class (serenity rest + hamcrest are used for assertions there)
3. Implement API calls steps in ReqresAPI to be used in definitions
4. For additional models like User use models catalog 
## API docs
### Create user endpoint
POST add user
https://reqres.in/api/users

`Content-Type = application/json`
##### Body raw data
`{
    "name": "{{name}}",
    "job": "{{job}}"
}`
##### 201 status code on success
`{
    "id": "{{id}}",
    "createdAt": "{{createdAt}}"
}`
##### Example Request
`curl --location --request POST 'https://reqres.in/api/users' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "{{name}}",
    "job": "{{job}}"
}'`

### Register user endpoint
POST register
https://reqres.in/api/register

Content-Type = application/json
##### Body raw data
`{
    "email": "{{email}}",
    "password": "{{password}}"
}`
##### 200 status code on success
`{
    "id": {{id}},
    "token": "{{token}}"
}`
##### 400 status code on failure
`{
    "error": "{{errorMessage}}"
}`
##### Example Request
`curl --location --request POST 'https://reqres.in/api/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "{{email}}",
    "password": "{{password}}"
}'`
